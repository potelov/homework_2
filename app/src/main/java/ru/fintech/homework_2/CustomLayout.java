package ru.fintech.homework_2;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;

public class CustomLayout extends ViewGroup {

    public CustomLayout(Context context) {
        this(context, null, 0);
    }

    public CustomLayout(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CustomLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int count = getChildCount();

        int parentWidth = MeasureSpec.getSize(widthMeasureSpec);

        int maxHeight;
        int currentWidth = 0;
        int childHeight = 0;
        int rowCount = count > 0 ? 1 : 0;

        for (int i = 0; i < count; i++) {

            final View child = getChildAt(i);

            if (child.getVisibility() != GONE) {
                int childWidth = child.getMeasuredWidth();
                measureChild(child, widthMeasureSpec, heightMeasureSpec);
                childHeight = child.getMeasuredHeight();
                currentWidth += childWidth;

                if (currentWidth > parentWidth) {
                    rowCount++;
                    currentWidth = childWidth;
                }
            }
        }

        maxHeight = childHeight * rowCount;
        setMeasuredDimension(parentWidth, maxHeight);
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        final int count = getChildCount();

        int curWidth, curHeight, maxHeight, curRight, curBottom;

        final int childRight = right - left - getPaddingRight();
        final int childBottom = bottom - top - getPaddingBottom();

        maxHeight = 0;
        curRight = childRight;
        curBottom = childBottom;

        for (int i = count - 1; i >= 0; i--) {
            View child = getChildAt(i);

            if (child.getVisibility() != GONE) {

                curWidth = child.getMeasuredWidth();
                curHeight = child.getMeasuredHeight();

                if (curRight - curWidth <= 0) {
                    curRight = childRight;
                    curBottom -= maxHeight;
                    maxHeight = 0;
                }

                child.layout(curRight - curWidth, curBottom - curHeight, curRight, curBottom);

                if (maxHeight < curHeight) {
                    maxHeight = curHeight;
                }
                curRight -= curWidth;
            }
        }
    }
}
