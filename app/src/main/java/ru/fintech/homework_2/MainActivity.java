package ru.fintech.homework_2;

import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private CustomLayout mTopLayout;
    private CustomLayout mBottomLayout;

    private List<Station> mStations = new ArrayList<Station>() {
        {
            add(new Station("Водный стадион", R.color.green));
            add(new Station("Сокол", R.color.green));
            add(new Station("Аэропорт", R.color.green));
            add(new Station("Динамо", R.color.green));
            add(new Station("Речной вокзал", R.color.green));

            add(new Station("Комсомольская", R.color.red));
            add(new Station("Локомотив", R.color.red));
            add(new Station("Сокольники", R.color.red));
            add(new Station("Чистые пруды", R.color.red));
            add(new Station("Лубянка", R.color.red));

            add(new Station("ВДНХ", R.color.orange));
            add(new Station("Проспект мира", R.color.orange));
            add(new Station("Ботанический сад", R.color.orange));
            add(new Station("Рижская", R.color.orange));
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setUp();
    }

    private void setUp() {
        mTopLayout = findViewById(R.id.top_layout);
        mBottomLayout = findViewById(R.id.bottom_layout);

        for (int i = 0; i < mStations.size(); i++) {
            View view = getLayoutInflater().inflate(R.layout.item_chips, null, false);
            view.setOnClickListener(this);
            TextView textView = view.findViewById(R.id.tv_name);
            final Station station = mStations.get(i);
            textView.setText(station.getName());
            textView.setTextColor(ContextCompat.getColor(this, station.getColor()));
            setTextViewDrawableColor(textView, station.getColor());
            if (i < mStations.size() / 2) {
                mTopLayout.addView(view);
            } else {
                mBottomLayout.addView(view);
            }
        }
    }

    private void setTextViewDrawableColor(TextView textView, int color) {
        for (Drawable drawable : textView.getCompoundDrawablesRelative()) {
            if (drawable != null) {
                drawable.setColorFilter(new PorterDuffColorFilter(ContextCompat.getColor(textView.getContext(), color), PorterDuff.Mode.SRC_IN));
            }
        }
    }

    @Override
    public void onClick(View view) {
        int id = ((ViewGroup) view.getParent()).getId();
        if (id == R.id.top_layout) {
            mTopLayout.removeView(view);
            mBottomLayout.addView(view);
        } else {
            mBottomLayout.removeView(view);
            mTopLayout.addView(view);
        }
    }
}
